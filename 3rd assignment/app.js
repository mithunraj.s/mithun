const app = Vue.createApp({
  data() {
    return {
      counter: 0,
    };
  },
  methods: {
    add(num) {
      this.counter = this.counter + num;
    },
  },
  computed: {
    cond() {
      if (this.counter < 37) {
        return "Not there yet";
      } else if (this.counter === 37) {
        return this.counter;
      } else {
        return "To much";
      }
    },
  },
  watch: {
    counter() {
      const that = this;
      if (that.counter > 0) {
        setTimeout(function () {
          that.counter = 0;
        }, 5000);
      }
    },
  },
});

app.mount("#assignment");
