const app = Vue.createApp({
  data() {
    return {
      uservalue: " ",
      goals: [],
    };
  },

  methods: {
    addgoal() {
      this.goals.push(this.uservalue);
      this.uservalue = " ";
    },
    removegoal(idx) {
      this.goals.splice(idx,1);
    },
  },
});

app.mount("#user-goals");
