const app = Vue.createApp({
  data() {
    return {
      uservalue:"",
       goals: [ ]
       };
  },
  methods:{
    newgoals(){
      this.goals.push(this.uservalue);
      this.uservalue=""
    },
    removeGoal(idx) {
      this.goals.splice(idx, 1);
    }
  }
});

app.mount('#user-goals');
