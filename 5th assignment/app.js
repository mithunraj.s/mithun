const app = Vue.createApp({
  data() {
    return {
      uservalue: "",
      task: [],
      show: true,
    };
  },
  computed: {
    buttoncaption() {
      return this.show ? "Hide_List" : "Show_List";
    },
  },
  methods: {
    addtask() {
      this.task.push(this.uservalue);
      this.uservalue = "";
    },
    hidetask() {
      this.show = !this.show;
    },
  },
});
app.mount("#assignment");
