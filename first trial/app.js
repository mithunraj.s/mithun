var vm= Vue.createApp({
    // el:'#app',
    data(){
        return{
            Goals:[],
            enteredValue:'',
        }
    },
    methods:{
        addGoal(){
            this.Goals.push(this.enteredValue)
            this.enteredValue=""
        }
    },
}).mount("#app");