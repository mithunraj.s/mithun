const app = Vue.createApp({
  data() {
    return {
      counter: 0,
      name: '',
      fullname:"",
      lastname:""
    };
  },
  computed:{
    // output(){
    //    console.log("computed");
    //   if(this.name ===""){
    //     return "";
    //   }
    //    return this.name+ "" +" "+ "raj";
    // }
  },
  watch:{
    name(value){
      console.log(value);
      if(value ===""){
        this.fullname="";
      }
      else{
     this.fullname= value+" "+this.lastname;
      }

    },
    lastname(value){
      console.log(value);
      if(value ===""){
        this.fullname="";
      }
      else{
     this.fullname= this.name+" "+value;
      }
    },
    counter(){
      if(this.counter>50){
        this.counter=0
            }
    }
     
  },
  methods: {
    // fullname(){
    //   // console.log("methods");
    //   if(this.name ===""){
    //     return "";
    //   }
    //    return this.name+ "" + "raj";
    // },
    reset(){
     this.counter =0
    },
    // setName(event) {
    //   this.name = event.target.value;
    // },
    add(num) {
      
      this.counter = this.counter + num;
    },
    reduce(num) {
      this.counter = this.counter - num;
      
    },
    resetinput(){
      this.name="";
      this.lastname="";
    }
  }
});

app.mount('#events');
