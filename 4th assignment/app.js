const app = Vue.createApp({
        data(){
            return{
                userinput:"",
                paragraph: true,
                background: ""
                     
            };
        },
        computed:{
          classes(){
              return{
            user1: this.userinput ==='user1',
            user2: this.userinput==='user2' ,
            visible: this.paragraph,
            hidden: !this.paragraph,
              }
          }
        },
        methods:{
            toggle(){
              this.paragraph = !this.paragraph;


            }
        }
});
app.mount("#assignment");